import pickle
import torch

from config_validator import SPLIT_FIELDS


def compute_experiment_data(evaluation_data, predictor_logits, uncertainty_data):
    accuracies, std_deviation = evaluate_predictor_metrics(predictor_logits)
    selective_risks = compute_selective_risks(evaluation_data, predictor_logits, uncertainty_data['scores'])
    expected_risks = compute_expected_risks(evaluation_data, predictor_logits, uncertainty_data['probabilities'])
    experiment_data = {"accuracies": accuracies, "deviation": std_deviation,
                       "selective_risks": selective_risks, "expected_risks": expected_risks}
    with open(evaluation_data['experiment_data_path'], "wb") as file:
        pickle.dump(experiment_data, file)


def evaluate_predictor_metrics(predictor_logits):
    accuracies, std_deviation = {}, {}
    for field in SPLIT_FIELDS:
        total = torch.zeros(len(predictor_logits.keys()))
        correct = torch.zeros(len(predictor_logits.keys()))
        for split_idx, split_logits in predictor_logits.items():
            for batch, labels in split_logits[field]:
                predictions = torch.argmax(batch, dim=1)
                total[split_idx] = len(labels)
                correct[split_idx] = torch.sum(predictions == labels)
        accuracies[field] = torch.sum(correct) / torch.sum(total)
        std_deviation[field] = (correct/total).std()
    return accuracies, std_deviation


def compute_selective_risks(evaluation_data, predictor_logits, uncertainty_scores):
    selective_risks = {}
    for split_field in SPLIT_FIELDS:
        selective_risks[split_field] = []
        for split_idx in range(evaluation_data['splits_num']):
            selective_risks[split_field].append(
                compute_selective_risk_for_subsplit(predictor_logits[split_idx][split_field],
                                                    uncertainty_scores[split_idx][split_field],
                                                    evaluation_data['true_loss']))
        selective_risks[split_field] = torch.stack(selective_risks[split_field], dim=0)
    return selective_risks


def compute_selective_risk_for_subsplit(split_logits, split_scores, true_loss):
    """
    Selective risk is a metric used to evaluate the performance of a selective classifier

    Selective classifier outputs
    0 (reject) for observations with uncertainty score > theta,
    1 (accept) for observations with uncertainty score <= theta

    The function outputs vector, where selective risk is computed for all possible thetas,
    (len(thetas) = len(raw_uncertainty_scores))
    """
    approximated_risks = []
    for batch, labels in split_logits:
        risk = true_loss(batch, labels)
        approximated_risks.extend(risk)

    uncertainty_scores = []  # s(x_i)
    for scores, _ in split_scores:
        uncertainty_scores.extend(scores)

    approximated_risks = torch.tensor(approximated_risks)  # l(y_i, h(x_i))
    uncertainty_scores = torch.tensor(uncertainty_scores)  # s(x_i)

    sorted_indices = torch.argsort(uncertainty_scores, descending=False).squeeze()
    sorted_risks = approximated_risks[sorted_indices]
    cumulative_risks = torch.cumsum(sorted_risks, dim=0)
    cumulative_coverage = torch.arange(1, len(cumulative_risks) + 1, dtype=torch.int)

    selective_risks = cumulative_risks / cumulative_coverage
    return selective_risks


def compute_expected_risks(evaluation_data, predictor_logits, probabilities):
    true_loss = evaluation_data['true_loss']
    expected_risks = {}
    for split_field in SPLIT_FIELDS:
        expected_risks[split_field] = []
        for split_idx in range(evaluation_data['splits_num']):
            if probabilities is not None:
                risk = get_expected_risk_for_subsplit(predictor_logits[split_idx][split_field], probabilities[split_idx][split_field], true_loss)
            else:
                risk = get_estimated_risk_for_subsplit(predictor_logits[split_idx][split_field], true_loss)
            expected_risks[split_field].append(risk)
        expected_risks[split_field] = torch.stack(expected_risks[split_field], dim=0)
    return expected_risks


def get_expected_risk_for_subsplit(subsplit_logits, subsplit_log_probs, true_loss):
    """
    Computes the expected conditional risk for batches of samples
    """
    expected_risks = []
    for (batch, labels), log_probs in zip(subsplit_logits, subsplit_log_probs):
        log_posteriors = log_probs - torch.logsumexp(log_probs, dim=1, keepdim=True)  # Shape: (batch_size, num_classes)

        # Compute the risk for all possible labels for the batch
        batch_risks = []
        for sample_idx in range(batch.shape[0]):
            sample_logits = batch[sample_idx]
            sample_log_posteriors = log_posteriors[sample_idx]  # Shape: (num_classes,)
            sample_losses = torch.tensor(
                [true_loss(sample_logits, torch.tensor([label])) for label in range(len(sample_logits))]
            )  # Shape: (num_classes,)

            # Compute the expected risk for this sample
            sample_expected_risk = torch.sum(torch.exp(sample_log_posteriors) * sample_losses)  # Scalar
            batch_risks.append(sample_expected_risk)

        # Append batch risks
        expected_risks.extend(batch_risks)

    # Sort expected risks in ascending order
    expected_risks = torch.tensor(expected_risks)
    expected_risks, _ = torch.sort(expected_risks, dim=0, descending=False)

    cumulative_risks = torch.cumsum(expected_risks, dim=0)
    cumulative_coverage = torch.arange(1, len(cumulative_risks) + 1, dtype=torch.int)

    expected_conditional_risks = cumulative_risks / cumulative_coverage
    return expected_conditional_risks


def get_estimated_risk_for_subsplit(subsplit_logits, true_loss):
    """
    Computes the expected conditional risk for batches of samples
    """
    estimated_risks = []
    for batch, _ in subsplit_logits:
        # Compute the risk for all possible labels for the batch
        batch_risks = []
        for logits_idx in range(batch.shape[0]):
            sample_logits = batch[logits_idx]
            sample_losses = torch.tensor([true_loss(sample_logits, torch.tensor([label])) for label in range(len(sample_logits))])  # Shape: (num_classes,)

            # Compute the expected risk for this sample
            sample_expected_risk = torch.sum(sample_losses * batch[logits_idx])  # Scalar
            batch_risks.append(sample_expected_risk)

        estimated_risks.extend(batch_risks)

    # Sort expected risks in ascending order
    estimated_risks = torch.tensor(estimated_risks)
    estimated_risks, _ = torch.sort(estimated_risks, dim=0, descending=False)

    cumulative_risks = torch.cumsum(estimated_risks, dim=0)
    cumulative_coverage = torch.arange(1, len(cumulative_risks) + 1, dtype=torch.int)

    expected_conditional_risks = cumulative_risks / cumulative_coverage
    return expected_conditional_risks

