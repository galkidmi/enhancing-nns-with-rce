from torch.utils.data import DataLoader, ConcatDataset, Subset
from sklearn.model_selection import StratifiedKFold
from torchvision import datasets

import pickle
import torch
import numpy as np

SUBFOLDERS_NUMBER = 100


class DeviceDataLoader:
    """
    Wraps a PyTorch DataLoader and moves data to the specified device.
    """
    def __init__(self, data_loader):
        self.data_loader = data_loader
        self.device = "cpu"

        self._choose_device()

    def __iter__(self):
        """Iterate through batches and move them to the device."""
        for batch in self.data_loader:
            yield self.batch_to_device(batch)

    def __len__(self):
        return len(self.data_loader)

    def batch_to_device(self, batch):
        """Move a batch of data to the device."""
        images, labels = batch
        return images.to(self.device), labels.to(self.device)

    def _choose_device(self):
        if torch.cuda.is_available():
            self.device = torch.device("cuda")
        else:
            self.device = torch.device("cpu")


class DatasetWrapper:
    def __init__(self, raw_dataset, raw_probabilities, num_classes):
        self.dataset = raw_dataset
        self.probabilities = raw_probabilities
        self.num_classes = num_classes


def fetch_raw_dataset(dataset_data):
    """
    Fetches a raw dataset (either built-in or custom) based on the given dataset name.
    """
    dataset_name = dataset_data['dataset_name']
    if hasattr(datasets, dataset_name):
        dataset_class = getattr(datasets, dataset_name)
        dataset_train = dataset_class(root=dataset_data['data_path'], train=True, download=True, transform=dataset_data['transformations'])
        dataset_test = dataset_class(root=dataset_data['data_path'], train=False, download=True, transform=dataset_data['transformations'])
        raw_dataset = ConcatDataset([dataset_train, dataset_test])
        raw_probabilities = None
        num_classes = len(set(dataset_train.targets))
    elif '.pkl' in dataset_data['data_path']:  # path to pickle file of synthetic dataset
        with open(dataset_data['data_path'], 'rb') as file:
            raw_dataset = pickle.load(file)
        raw_probabilities = raw_dataset.probabilities
        num_classes = len(set(raw_dataset.labels))

    else:
        raw_dataset = datasets.ImageFolder(root=dataset_data['data_path'], transform=dataset_data['transformations'])
        raw_probabilities = None
        num_classes = len(raw_dataset.classes)

    return DatasetWrapper(raw_dataset, raw_probabilities, num_classes)


def prepare_dataset(dataset_data, wrapped_raw_dataset):
    """
    Constructs stratified dataset
    """
    stratified_dataset, stratified_probabilities = perform_stratification(wrapped_raw_dataset)
    splits = assign_subfolders_to_splits(dataset_data)
    dataset = get_dataset_loaders(dataset_data['batch_size'], stratified_dataset, splits)
    probabilities = get_probabilities_loaders(dataset_data['batch_size'], stratified_probabilities, splits)
    return DatasetWrapper(dataset, probabilities, wrapped_raw_dataset.num_classes)


def perform_stratification(wrapped_dataset):
    raw_dataset = wrapped_dataset.dataset
    raw_probabilities = wrapped_dataset.probabilities

    dataset_indices = torch.arange(len(raw_dataset))
    labels = torch.tensor([raw_dataset[i][1] for i in range(len(raw_dataset))])
    skf = StratifiedKFold(n_splits=SUBFOLDERS_NUMBER, shuffle=False)

    stratified_dataset = {idx: Subset(raw_dataset, subset_indices)
                        for idx, (_, subset_indices) in enumerate(skf.split(dataset_indices, labels))}

    stratified_probabilities = {idx: Subset(raw_probabilities, subset_indices)
                        for idx, (_, subset_indices) in enumerate(skf.split(dataset_indices, labels))} \
                            if raw_probabilities is not None else None

    return stratified_dataset, stratified_probabilities


def get_dataset_loaders(batch_size, stratified_dataset, splits):
    """
    Creates a dictionary of DeviceDataLoader objects in format "loader name : data loader" for each split,
    where each data loader is constructed of multiple concatenated subset objects, which correspond to the subfolders
    defined during stratified k sampling.
    """
    splitted_dataset = {}
    for split_idx, split_def in splits.items():
        splitted_dataset[split_idx] = {}
        for loader_name, subfolds_indices in split_def.items():
            dataset_for_loader = ConcatDataset([stratified_dataset[subfold_idx] for subfold_idx in subfolds_indices])
            splitted_dataset[split_idx][loader_name] = DeviceDataLoader(DataLoader(dataset_for_loader, batch_size, False))
    return splitted_dataset


def get_probabilities_loaders(batch_size, stratified_dataset, splits):
    """
    Creates a dictionary of DataLoader objects in format "loader name : data loader" for each split,
    where each data loader is constructed of multiple concatenated subset objects, which correspond to the subfolders
    defined during stratified k sampling.
    """
    if stratified_dataset is None:
        return None

    splitted_dataset = {}
    for split_idx, split_def in splits.items():
        splitted_dataset[split_idx] = {}
        for loader_name, subfolds_indices in split_def.items():
            dataset_for_loader = ConcatDataset([stratified_dataset[subfold_idx] for subfold_idx in subfolds_indices])
            splitted_dataset[split_idx][loader_name] = DataLoader(dataset_for_loader, batch_size, False)
    return splitted_dataset


def assign_subfolders_to_splits(dataset_data, seed=42):
    """
    Assigns subfolders to training, validation, and testing splits for cross-validation.
    """
    np.random.seed(seed)

    split_keys = list(dataset_data['split_percentages'].keys())
    split_sizes = list(dataset_data['split_percentages'].values())
    start_indices = np.cumsum([0] + split_sizes[:-1])
    init_start_inds = dict(zip(split_keys, start_indices))

    step_size = SUBFOLDERS_NUMBER // dataset_data['splits_number']
    all_folders = np.arange(SUBFOLDERS_NUMBER)
    splits = {}
    limit = None

    for split_idx in range(dataset_data['splits_number']):
        np.random.shuffle(all_folders)

        assigned_folders = {}
        start_idx = split_idx * step_size
        for key, subsplit_size in dataset_data['split_percentages'].items():
            if start_idx == init_start_inds[key] and split_idx > 0:
                limit = len(assigned_folders.get(key, []))
                print(f"Warning: The number of splits was changed to {limit}, since it's initial value was too high")
                break

            end_idx = start_idx + subsplit_size
            assigned_folders[key] = list(all_folders[start_idx:end_idx])

            if end_idx >= len(all_folders):
                overflow = end_idx - len(all_folders)
                assigned_folders[key].extend(all_folders[:overflow].tolist())
                start_idx = overflow
            else:
                start_idx = end_idx
        splits[split_idx] = assigned_folders

    if limit is not None:
        for split_idx, split_def in splits.items():
            for key, subsplit_def in split_def.items():
                splits[split_idx][key] = subsplit_def[:limit]
    return splits


