from torchvision.transforms import transforms
from torch import optim, nn
import torch
import yaml

import true_loss_functions as tlf
import uncertainty_models as um
import predictor_models as pm


DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def fetch_raw_dataset_data(config, paths):
    dataset_data = {
         'dataset_name': config['data_management']['dataset_name'],
         'data_path': paths["data_path"],
         'transformed_data_path': paths["transformed_data_path"],  # 1 usage
    }
    if "transformations" in config['data_management']:
        dataset_data['transformations'] = construct_pipeline(config['data_management']["transformations"])
    return dataset_data


def fetch_dataset_data(config, config_part_name):
    if isinstance(config['data_management']['split_percentages'], list):
        pass  # TODO: finish
    else:
        return {
            "splits_number": config['data_management']["splits_number"],
            "split_percentages": config['data_management']["split_percentages"],
            "batch_size": config[config_part_name]["batch_size"]
        }


def construct_pipeline(config_preprocessing):
    """
    Constructs a transformation pipeline based on the configuration.
    """
    transform_list = [transforms.ToTensor()]

    if 'transformations' not in config_preprocessing:
        return transforms.Compose(transform_list)

    transformations = config_preprocessing['transformations']
    for transformation in transformations:
        name = transformation['name']
        params = transformation.get('params', {})

        transform = getattr(transforms, name)  # existance is guaranteed by config_validator
        if params:
            transform_list.append(transform(**params))
        else:
            transform_list.append(transform())

    return transforms.Compose(transform_list)


def fetch_predictor_data(config, paths, dataset, num_classes):
    predictor = get_predictor(config["train_predictor"]["architecture"], dataset[0]['test'], num_classes)

    return {
        "predictor_logits_path": paths["predictor_logits_path"],
        "true_loss": get_true_loss(config["train_predictor"]["true_loss"], num_classes),
        "model": predictor,
        "optimizer": get_optimizer(config, predictor),
        "loss_function": getattr(nn, config["train_predictor"]["loss_function"])(reduction='mean'),
        "max_epochs": config["train_predictor"]["max_epochs"],
        "patience": config["train_predictor"]["patience"],
    }


def fetch_uncertainty_data(config, paths, dataset, num_classes):
    if config["train_uncertainty"]["architecture"] == "MCP_score":
        return {
            'uncertainty_scores_path': paths["uncertainty_scores_path"],
            "model": um.MCP_score,
        }
    else:
        uncertainty_model = get_uncertainty_model(config, dataset[0]['test'])
        return {
            'uncertainty_scores_path': paths["uncertainty_scores_path"],
            "model": uncertainty_model,
            "loss_function_name": config["train_uncertainty"]["loss_function"],
            "loss_function": getattr(um, config["train_uncertainty"]["loss_function"]),
            "true_loss": get_true_loss(config["train_uncertainty"]["true_loss"], num_classes),
            "optimizer": get_optimizer(config, uncertainty_model),
            "max_epochs": config["train_uncertainty"]["max_epochs"],
            "patience": config["train_uncertainty"]["patience"]
        }


def get_predictor(architecture, sample, num_classes):
    channels, height, width = get_sample_size(sample)
    predictor_class = getattr(pm, architecture)
    predictor = predictor_class(channels, height, width, num_classes).to(DEVICE)
    return predictor


def get_optimizer(config, model):
    lr = config["train_predictor"]["learning_rate"]
    optimizer_name = config["train_predictor"]["optimizer"]
    optimizer_class = getattr(optim, optimizer_name)
    optimizer = optimizer_class(model.parameters(), lr=lr)
    return optimizer


def get_sample_size(loader):
    batch, _ = next(iter(loader))  # take first sample from batch
    shape = list(batch.shape)[1:]
    if len(shape) == 3:
        return shape
    elif len(shape) == 2:
        return 1, shape[0], shape[1]
    elif len(shape) == 1:
        return 1, 1, shape[0]
    else:
        raise Exception('Unkown data sample format with 3< dimensions')


def get_true_loss(true_loss, num_classes):
    if not hasattr(tlf, true_loss):  # true loss is defined as a matrix
        with open(true_loss, "r") as f:
            matrix = torch.tensor(yaml.safe_load(f))
        if len(matrix) != num_classes or len(matrix[0]) != num_classes:
            message = "Invalid matrix definition! Matrix must be quadratice, where n = number of classes!"
            raise ValueError(message)
        return tlf.MatrixLoss(matrix)
    else:
        return getattr(tlf, true_loss)


def get_uncertainty_model(config, sample):
    channels, height, width = get_sample_size(sample)
    uncertainty_model_class = getattr(um, config["train_uncertainty"]["architecture"])
    if config["train_uncertainty"]["loss_function"] == 'TCP_score':
        activation = nn.Sigmoid
    else:
        activation = nn.Softplus
    uncertainty_model = uncertainty_model_class(channels, height, width, activation).to(DEVICE)
    return uncertainty_model


def fetch_evaluation_data(config, paths):
    return {
        'splits_num': config['data_management']['splits_number'],
        "experiment_data_path": paths["experiment_data_path"],
        "true_loss": getattr(tlf, config["train_uncertainty"]["true_loss"])
    }


def fetch_html_part(config, paths):
    html_part = {
        'html_results_path': paths['html_results_path'],
        'dataset_name': config['data_management']['dataset_name'],
        'splits_num': config['data_management']['splits_number'],
        'predictor_name': config['train_predictor']['architecture'],
        'uncertainty_name': config['train_uncertainty']['architecture'],
        'true_loss': config['train_uncertainty']['true_loss']
    }

    if isinstance(html_part['uncertainty_name'], str) and html_part['uncertainty_name'] != "MCP_score":
        html_part['uncertainty_name'] = f"{html_part['uncertainty_name']}({config['train_uncertainty']['loss_function']})"

    return html_part


def fetch_complex_html_part(config, um_params, html_results_path, order):
    complex_html_part = fetch_html_part(config, {"html_results_path": html_results_path})
    complex_html_part['order'] = order
    complex_html_part = add_multiple_um_params(config, complex_html_part, um_params)
    return complex_html_part


def add_multiple_um_params(config, complex_html_part, um_params):
    if len(um_params) == 2:
        complex_html_part['uncertainty_name'] = construct_um_models_names(um_params)
    elif um_params[0]['name'] == 'train_uncertainty.architecture':
        um_names = ", ".join(complex_html_part['uncertainty_name'])
        complex_html_part['uncertainty_name'] = f"{um_names} + {config['train_uncertainty']['loss_function']}"
    elif um_params[0]['name'] == 'train_uncertainty.loss_function':
        um_names = ", ".join(config['train_uncertainty']['loss_function'])
        complex_html_part['um_param_info'] = {'name': "um loss functions", 'values': um_names}
    elif um_params[0]['name'] == 'train_uncertainty.learning_rate':
        um_lrs = ", ".join(config['train_uncertainty']['learning_rate'])
        complex_html_part['um_param_info'] = {'name': "um learning rates", 'values': um_lrs}
    return complex_html_part


def construct_um_models_names(um_params):
    if um_params[0]['name'] == "train_uncertainty.architecture":
        names = um_params[0]["values"]
        loss_functions = um_params[1]["values"]
    else:
        names = um_params[1]["values"]
        loss_functions = um_params[0]["values"]

    um_names = [f"{name}({loss_function})" for name in names for loss_function in loss_functions if name != 'MCP_score']
    if 'MCP_score' in names:
        um_names.append('MCP_score')
    return ", ".join(um_names), um_names


