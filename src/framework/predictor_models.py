import torch.nn as nn
import torch


class CNN_Large(nn.Module):
    def __init__(self, channels, height, width, num_classes):
        super(CNN_Large, self).__init__()

        self.conv_layers = nn.Sequential(
            nn.Conv2d(channels, 64, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(64, 128, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(128, 256, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(256, 256, kernel_size=3, padding=1),
            nn.ReLU()
        )

        in_features = compute_in_features(self.conv_layers, channels, height, width)

        self.fc_layers = nn.Sequential(
            nn.Linear(in_features, 512),
            nn.ReLU(),
            nn.Linear(512, 256),
            nn.ReLU(),
            nn.Linear(256, num_classes)
        )

    def forward(self, x):
        x = self.conv_layers(x)
        x = x.view(x.size(0), -1)
        x = self.fc_layers(x)
        return x


class CNN_medium(nn.Module):
    def __init__(self, channels, height, width, num_classes):
        super(CNN_medium, self).__init__()

        self.conv_layers = nn.Sequential(
            nn.Conv2d(channels, 32, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(64, 128, kernel_size=3, padding=1),
            nn.ReLU()
        )

        in_features = compute_in_features(self.conv_layers, channels, height, width)

        self.fc_layers = nn.Sequential(
            nn.Linear(in_features, 256),
            nn.ReLU(),
            nn.Linear(256, num_classes)
        )

    def forward(self, x):
        x = self.conv_layers(x)
        x = x.view(x.size(0), -1)
        x = self.fc_layers(x)
        return x


class CNN_Small(nn.Module):
    def __init__(self, channels, height, width, num_classes):
        super(CNN_Small, self).__init__()

        self.conv_layers = nn.Sequential(
            nn.Conv2d(channels, 16, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv2d(16, 32, kernel_size=3, padding=1),
            nn.ReLU()
        )

        in_features = compute_in_features(self.conv_layers, channels, height, width)

        self.fc_layers = nn.Sequential(
            nn.Linear(in_features, 128),
            nn.ReLU(),
            nn.Linear(128, num_classes)
        )

    def forward(self, x):
        x = self.conv_layers(x)
        x = x.view(x.size(0), -1)
        x = self.fc_layers(x)
        return x


def compute_in_features(conv_layers, input_channels, height, width):
    # Create a dummy input tensor with the specified dimensions
    dummy_input = torch.zeros(1, input_channels, height, width)

    try:
        output = conv_layers(dummy_input)
        return output.view(1, -1).size(1)
    except RuntimeError as e:
        raise ValueError(
            f"Input dimensions (height={height}, width={width}) are too small for the network architecture. "
            f"Consider changing the predictor or dataset."
        )


class FCN_Large(nn.Module):
    def __init__(self, channels, height, width, num_classes):
        super(FCN_Large, self).__init__()

        in_features = channels * height * width

        self.fc_layers = nn.Sequential(
            nn.Linear(in_features, 512),
            nn.ReLU(),
            nn.Linear(512, 256),
            nn.ReLU(),
            nn.Linear(256, 128),
            nn.ReLU(),
            nn.Linear(128, 64),
            nn.ReLU(),
            nn.Linear(64, num_classes)
        )

    def forward(self, x):
        x = x.view(x.size(0), -1)
        logits = self.fc_layers(x)
        return logits


class FCN_medium(nn.Module):
    def __init__(self, channels, height, width, num_classes):
        super(FCN_medium, self).__init__()

        in_features = channels * height * width

        self.fc_layers = nn.Sequential(
            nn.Linear(in_features, 256),
            nn.ReLU(),
            nn.Linear(256, 128),
            nn.ReLU(),
            nn.Linear(128, 64),
            nn.ReLU(),
            nn.Linear(64, num_classes)
        )

    def forward(self, x):
        x = x.view(x.size(0), -1)
        logits = self.fc_layers(x)
        return logits


class FCN_Simple(nn.Module):
    def __init__(self, channels, height, width, num_classes):
        super(FCN_Simple, self).__init__()

        in_features = channels * height * width

        self.fc_layers = nn.Sequential(
            nn.Linear(in_features, 32),
            nn.ReLU(),
            nn.Linear(32, 64),
            nn.ReLU(),
            nn.Linear(64, num_classes)
        )

    def forward(self, x):
        x = x.view(x.size(0), -1)
        logits = self.fc_layers(x)
        return logits


