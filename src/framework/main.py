import experiment_manager as em
import synthetic_dataset_generator as sdg


def run_experiment(exp_num):
    if exp_num == 0:
        config_path = "/Users/dmitryygalkin/Desktop/semester_5/Project/Diploma/configuration_files/experiments/new_config.yaml"
        em.run_experiment(config_path)
    elif exp_num == 1:
        config_path = "/Users/dmitryygalkin/Desktop/semester_5/Project/Diploma/configuration_files/dataset_generation/dataset1.yaml"
        sdg.generate_synthetic_dataset(config_path)


if __name__ == '__main__':
    run_experiment(0)




