from torchvision.transforms import transforms
from torchvision import datasets
import torch.optim as optim
import torch.nn as nn
import os

import true_loss_functions as tlf
import uncertainty_models as um
import predictor_models as pm

REQUIRED_FIELDS = {
    'root',
    'data_management',
    'data_management.dataset_name',
    'data_management.data_path',
    'data_management.results_path',
    'data_management.splits_number',
    'data_management.split_percentages',
    'train_predictor',
    'train_predictor.architecture',
    'train_predictor.batch_size',
    'train_predictor.true_loss',
    'train_predictor.optimizer',
    'train_predictor.loss_function',
    'train_predictor.learning_rate',
    'train_predictor.max_epochs',
    'train_predictor.patience',
    'train_uncertainty',
    'train_uncertainty.architecture',
    'train_uncertainty.batch_size',
    'train_uncertainty.true_loss'
}

REQUIRED_NN_UNCERTAINTY_FIELDS = {
    'train_uncertainty.optimizer',
    'train_uncertainty.loss_function',
    'train_uncertainty.learning_rate',
    'train_uncertainty.max_epochs',
    'train_uncertainty.patience'
}

OPTIONAL_FIELDS = {
    'data_management.split_percentages.trn1',
    'data_management.split_percentages.val1',
    'data_management.split_percentages.trn2',
    'data_management.split_percentages.val2',
    'data_management.split_percentages.test',
    'evaluation',
    'evaluation.html_output'
}

SPLIT_FIELDS = ["trn1", "val1", "trn2", "val2", "test"]


COMPARISON_PARAMS = [  # uncertainty model parameters
    'train_uncertainty.architecture',
    'train_uncertainty.loss_function',
    'train_uncertainty.batch_size',
    'train_uncertainty.true_loss'
]


# ============================================== Validate Config Structure =============================================
def validate_config_structure(flat_config):
    """
    Ensures that configuration file has correct structure:
    - all required fields exists
    - there is no fields that are not supported by a framework
    """
    requiered_fields = REQUIRED_FIELDS
    required_nn_uncertainty_fields = REQUIRED_NN_UNCERTAINTY_FIELDS
    if flat_config['train_uncertainty.architecture'] != "MCP_score":
        requiered_fields = requiered_fields.union(required_nn_uncertainty_fields)

    optional_fields = requiered_fields | OPTIONAL_FIELDS

    synthetic_data_path = os.path.join(flat_config['data_management.data_path'], f"{flat_config['data_management.data_path']}.pkl")
    if not os.path.exists(synthetic_data_path):
        if 'data_management.transformations' in flat_config:
            validate_transformations_section_structure(flat_config['data_management.transformations'])
        optional_fields.add('data_management.transformations')
    check_fields(requiered_fields, optional_fields, set(flat_config.keys()))


def validate_transformations_section_structure(transformations):
    if not isinstance(transformations, list):
        message = f"Parametr data_management.transformations must be a list of dictionaries with keys (name, params)"
        raise ValueError(message)

    required_fields = {'name'}
    optional_fields = {'name', 'params'}
    for transformation in transformations:
        check_fields(required_fields, optional_fields, set(transformation.keys()))


def check_fields(requiered_fields, optional_fields, actual_fields):
    missing_fields = requiered_fields - actual_fields
    unsupported_fields = actual_fields - optional_fields
    if missing_fields:
        message = f"Missing required fields: {', '.join(missing_fields)}."
        raise KeyError(message)
    if unsupported_fields:
        massage = f"Following fields: {', '.join(unsupported_fields)} are not supported."
        raise KeyError(massage)


# =============================================== Validate Config Content ==============================================
def validate_config_content(flat_config):
    validate_paths(flat_config)
    validate_dataset(flat_config)
    validate_splits(flat_config)
    validate_quantitative_parameters(flat_config)
    validate_other_parameters(flat_config)
    validate_optional_parameters(flat_config)


def validate_paths(config):
    dataset_path = config["data_management.data_path"]
    if not os.path.exists(dataset_path):
        message = f"Path to the dataset does not exist: {dataset_path}"
        raise ValueError(message)

    results_path = config["data_management.results_path"]
    if not os.path.exists(results_path):
        message = f"Path to the results folder does not exist: {results_path}"
        raise ValueError(message)


def validate_dataset(flat_config):
    name = flat_config['data_management.dataset_name']

    synthetic_data_path = os.path.join(flat_config['data_management.data_path'], f"{flat_config['data_management.data_path']}.pkl")
    if not hasattr(datasets, name):
        if not os.path.exists(synthetic_data_path) and not os.path.exists(flat_config["data_management.data_path"]):
            #  ImageFolder (The name of the dataset in this case is arbitrary and the path to the dataset must exist)
            message = f"Dataset with name {name} does not exist..."
            raise ValueError(message)


def validate_splits(flat_config):
    """
    Validates split definitions ensuring all fields exist and percentages sum to 100.
    """
    def _validate_single_split(split_definition):
        """
        Helper function to validate a single split definition.
        """
        if not isinstance(split_definition, dict):
            raise ValueError("Each split definition must be a dictionary.")

        missing_fields = [field for field in SPLIT_FIELDS if field not in split_definition]
        if missing_fields:
            raise ValueError(f"Invalid split definition! Missing fields: {missing_fields}")

        if any(not isinstance(split_definition[field], int) or not (0 <= split_definition[field] <= 100) for field in SPLIT_FIELDS):
            raise ValueError("Invalid percentage definition! Each field must be an integer between 0 and 100.")

        if sum(split_definition[field] for field in SPLIT_FIELDS) != 100:
            raise ValueError("Invalid split definition! Percentages must sum up to 100.")

    # Handle case where multiple split definitions are given
    split_key = 'data_management.split_percentages'
    if isinstance(flat_config.get(split_key), list):
        for split_definition in flat_config[split_key]:
            _validate_single_split(split_definition)
    else:
        # Handle case where split percentages are defined as separate keys
        single_split = {field: flat_config.get(f"{split_key}.{field}") for field in SPLIT_FIELDS}
        _validate_single_split(single_split)


def validate_quantitative_parameters(config):
    positive_params = {
        'data_management.splits_number': (0, None, "Number of splits must be a positive integer"),
        'train_predictor.batch_size': (0, None, "batch_size must be a positive number or an array of positive numbers!"),
        'train_predictor.learning_rate': (0, None, "learning_rate must be a positive number!"),
        'train_predictor.max_epochs': (0, None, "max_epochs must be a positive number!"),
        'train_predictor.patience': (0, None, "patience must be a positive number!"),
        'train_uncertainty.batch_size': (0, None, "batch_size must be a positive number or an array of positive numbers!"),
        'train_uncertainty.learning_rate': (0, None, "learning_rate must be a positive number or an array of positive numbers!"),
        'train_uncertainty.max_epochs': (0, None, "max_epochs must be a positive number!"),
        'train_uncertainty.patience': (0, None, "patience must be a positive number!")
    }

    for param_name, (min_value, max_value, error_message) in positive_params.items():
        if param_name not in config:  # for optional parameters
            continue

        param = config[param_name]

        # Validate parameter type and positivity
        if isinstance(param, (int, float)):
            if param <= min_value or (max_value is not None and param > max_value):
                raise ValueError(error_message)
        elif isinstance(param, list) and param_name == 'train_uncertainty.batch_size':
            if not all(isinstance(x, (int, float)) and x > min_value for x in param):
                raise ValueError(error_message)
            if max_value is not None and not all(x <= max_value for x in param):
                raise ValueError(error_message)
        elif param_name in COMPARISON_PARAMS:
            raise ValueError(f"{param_name} must be a positive number or a list of positive numbers.")
        else:
            raise ValueError(f"{param_name} must be a positive number.")


def validate_other_parameters(config):
    pairs_to_check = [
        # source, name, can_be_an_array
        (pm, 'train_predictor.architecture', False),
        (um, 'train_uncertainty.architecture', False),
        (optim, 'train_predictor.optimizer', False),
        (nn, 'train_predictor.loss_function', False),
        (tlf, 'train_predictor.true_loss', False),
        (tlf, 'train_uncertainty.true_loss', True)
    ]

    if config['train_uncertainty.architecture'] != 'MCP_score':
        pairs_to_check.append((um, 'train_uncertainty.loss_function', True))
        pairs_to_check.append((optim, 'train_uncertainty.optimizer', False))

    for source, name, probable_array in pairs_to_check:
        if not isinstance(config[name], list):
            if not (("true_loss" in name and isinstance(config[name], str) and os.path.exists(config[name])) or hasattr(source, config[name])):
                message = f"Parameter {name} is invalid. It must be defined as a matrix or be defined in true_loss_functions module"
                raise ValueError(message)
        elif probable_array:
            for param in config[name]:
                if not hasattr(source, param):
                    message = f"One of the array parameters of {name} is not valid: {param}"
                    raise ValueError(message)
        else:
            message = f"Parameter {name} cannot be defined as an array!"
            raise ValueError(message)


def validate_optional_parameters(config):
    if 'data_management.transformations' in config:
        transformations = config['data_management.transformations']
        for transformation in transformations:
            name = transformation['name']

            if not hasattr(transforms, name):
                message = f"Unknown dataset transformation {name} was found"
                raise ValueError(message)

    if 'evaluation.html_output' in config and not isinstance(config['evaluation.html_output'], bool):
        message = f"Parametr evaluation.html_output must be boolean"
        raise ValueError(message)


# ============================================== Get Validated Parameters ==============================================
def get_comparison_parameters(config):
    comp_params = get_parameters(config, COMPARISON_PARAMS)

    extracted_names = [item['name'] for item in comp_params]
    if len(comp_params) > 1 and not extracted_names == ['train_uncertainty.architecture', 'train_uncertainty.loss_function']:
        message = f"Only one uncertainty model training parameter ({COMPARISON_PARAMS}) can be defined as an array!"
        raise ValueError(message)

    return comp_params


def get_parameters(config, keys):
    parameters = []
    for key in keys:
        if key in config and isinstance(config[key], list):
            if len(config[key]) == len(set(config[key])):  # array contains unique values
                dict_params = {'name': key, 'values': config[key]}
                parameters.append(dict_params)
            else:
                message = f"Array of parameters in {key} must contain unique values!"
                raise ValueError(message)
    return parameters


def get_configs_execution_order(config):  # TODO: rewrite complete. Prepare data for an html output
    """
    Generates the execution order based on environment and uncertainty model parameters.
    """
    um_params = get_parameters(config, COMPARISON_PARAMS)
    pairs = get_order(um_params)

    if len(um_params) == 2:
        um_params[0]['name'] = 'uncertainty model'

    order = []
    for um_value in pairs:
        if um_value is not None:
            conf = f"uncertainty model {um_params[0]['name'].split('.')[-1].replace('_', ' ')}: {um_value}"
        else:
            conf = ""
        order.append(conf)
    return order


def get_order(um_params):
    order = []

    if len(um_params) == 2:
        # Identify names and loss functions
        names, loss_functions = (
            (um_params[0]["name"].split('.')[-1], um_params[1]["name"].split('.')[-1])
            if um_params[0]['name'] == "train_uncertainty.architecture"
            else (um_params[1]["name"].split('.')[-1], um_params[0]["name"].split('.')[-1])
        )
        for name in names:
            for loss_function in loss_functions:
                model_name = f"{name}({loss_function})"
                order.append(model_name)
    elif len(um_params) == 1:
        for um_value in um_params[0]['values']:
            order.append(um_value)
    return order



