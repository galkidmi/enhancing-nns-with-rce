import torch
from numpy.random import multivariate_normal
from torch.utils.data import Dataset
import yaml
from scipy.stats import multivariate_normal
import pickle
import numpy as np
import random
from sklearn.preprocessing import StandardScaler


class SyntheticDataset(Dataset):
    def __init__(self, config, seed=42):
        data, labels, probabilities = generate_dataset(config, seed)
        self.data = data
        self.labels = labels
        self.probabilities = Probabilities(probabilities)

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        return self.data[idx], self.labels[idx]


class Probabilities(Dataset):
    def __init__(self, probabilities):
        self.probabilities = probabilities

    def __len__(self):
        return len(self.probabilities)

    def __getitem__(self, idx):
        return self.probabilities[idx]


def generate_synthetic_dataset(config_path):
    config = load_config(config_path)
    check_config_content(config)
    dataset = SyntheticDataset(config)
    with open(config['result_path'], 'wb') as file:
        pickle.dump(dataset, file)


def check_config_content(config):
    required_fields = {"dimension", "samples_per_class", "number_of_classes"}

    missing_fields = required_fields - set(config.keys())
    if missing_fields:
        raise ValueError("Invalid fields in config for dataset generation")

    if config["dimension"] < 2 or config["dimension"] > 8:
        raise ValueError("Entered dimension of samples is not supported!")
    if config["samples_per_class"] < 10 or config["samples_per_class"] > 10000000:
        raise ValueError("Entered samples_per_class parameter is not supported!")
    if config["number_of_classes"] < 1 or config["number_of_classes"] > 100:
        raise ValueError("Entered number_of_classes parameter is not supported!")


def set_seed(seed):
    """Set random seed for reproducibility"""
    np.random.seed(seed)
    torch.manual_seed(seed)
    random.seed(seed)


def generate_dataset(config, seed):
    set_seed(seed)

    data = []
    labels = []
    probabilities = []
    pdfs = []

    num_classes = config["number_of_classes"]
    dimension = config["dimension"]
    samples_per_class = config["samples_per_class"]

    step = 1 / (num_classes + 1)

    # Create a fixed random rotation matrix for reproducibility
    rng = np.random.default_rng(seed)  # Use NumPy’s recommended RNG
    rotation_matrix = np.linalg.qr(rng.standard_normal((dimension, dimension)))[0]

    for label in range(num_classes):
        mean = rng.uniform(label * step, (label + 1) * step, size=dimension)
        var = rng.uniform(0.5, 1 + (num_classes / 20), size=dimension)

        # Use the seeded random generator for reproducibility
        points = rng.multivariate_normal(mean, np.diag(var), size=samples_per_class)
        points = np.dot(points, rotation_matrix)
        points += 0.1 * np.sin(points * 2 * np.pi)

        pdfs.append(multivariate_normal(mean=mean, cov=np.diag(var)))
        data.append(points)
        labels.append(np.full(samples_per_class, label))

    for labeled_subdataset in data:
        subdataset_probs = []
        for label in range(num_classes):
            subdataset_probs.append(pdfs[label].logpdf(labeled_subdataset))
        probabilities.append(np.vstack(subdataset_probs).astype(np.float32).T)

    data = np.vstack(data).astype(np.float32)
    labels = np.hstack(labels)
    probabilities = np.vstack(probabilities).astype(np.float32)

    scaler = StandardScaler()
    data = scaler.fit_transform(data)

    shuffled_indices = rng.permutation(len(data))  # Use seeded permutation

    return (
        torch.tensor(data[shuffled_indices], dtype=torch.float32),
        torch.tensor(labels[shuffled_indices], dtype=torch.long),
        torch.tensor(probabilities[shuffled_indices], dtype=torch.float32),
    )


def load_config(config_path):
    """Loads the YAML configuration file."""
    try:
        with open(config_path, 'r') as file:
            return yaml.safe_load(file)
    except Exception as e:
        raise RuntimeError(f"Failed to load configuration from {config_path}: {e}")


