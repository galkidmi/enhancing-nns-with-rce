import copy
from datetime import datetime
from torchvision import datasets

import hashlib
import yaml
import os


def fetch_prepared_index(results_folders_path):
    """
    Prepares the index file by creating it if it does not exist or updating
    its structure if it does.
    """
    index_path = os.path.join(results_folders_path, "index.yaml")

    if not os.path.exists(index_path):
        index = {"data": {}, "predictor": {}, "uncertainty": {}, "evaluation": {}}
    else:
        with open(index_path, 'r') as f:
            index = yaml.safe_load(f)
        index["data"] = clean_invalid_paths(index.get("data", {}))  # paths to transformed_data.pkl
        index["predictor"] = clean_invalid_paths(index.get("predictor", {}))  # paths to predictor_logits.pkl
        index["uncertainty"] = clean_invalid_paths(index.get("uncertainty", {}))  # paths to uncertainty_scores.pkl
        index["evaluation"] = clean_invalid_paths(index.get("evaluation", {}))  # paths to experiment_data.pkl

    with open(index_path, 'w') as f:
        yaml.dump(index, f, default_flow_style=False)

    return index


def clean_invalid_paths(index_part):
    """
    Ensures that all paths in the index exist.
    """
    return {hash_key: path for hash_key, path in index_part.items() if os.path.exists(path)}


def prepare_result_paths(config_path, config, simple_configs):
    """
    Creates folder for the experiment and also create folders for subexperiments
    """
    result_folder_path = construct_result_folder_path(config_path, config["data_management"]["results_path"])
    for idx, simple_config in enumerate(simple_configs):
        simple_config["data_management.results_path"] = result_folder_path
    html_results_path = os.path.join(result_folder_path, 'result.html')
    return html_results_path


def construct_result_folder_path(config_path, results_folders_path):
    if os.path.exists(config_path):  # real config path, call from the complex experiment
        experiment_name = str(os.path.splitext(os.path.basename(config_path))[0])
    else:  # already prepared name, call from the simple experiment
        experiment_name = config_path

    timestamp = datetime.now().strftime("%H:%M-%d.%m.%Y")
    folder_name = f"{experiment_name}({timestamp})"
    result_folder_path = os.path.join(results_folders_path, folder_name)
    os.makedirs(result_folder_path, exist_ok=True)
    return result_folder_path


def construct_paths(config_path, config):
    results_folders_path = config["data_management"]["results_path"]
    result_folder_path = construct_result_folder_path(config_path, results_folders_path)

    dataset_name = str(config['data_management']['dataset_name'])
    if hasattr(datasets, dataset_name):
        data_path = config["data_management"]["data_path"]
    elif os.path.exists(os.path.join(config["data_management"]["data_path"], f"{dataset_name}.pkl")):
        data_path = str(os.path.join(config["data_management"]["data_path"], f"{dataset_name}.pkl"))
    else:
        data_path = config["data_management"]["data_path"]

    if os.path.exists(config_path):  # real config path -> call is done for a simple experiment
        index_path = os.path.join(results_folders_path, "index.yaml")
    else:  # already prepared name -> call is done for a complex experiment
        parent_directory = os.path.dirname(results_folders_path)
        index_path = os.path.join(parent_directory, "index.yaml")

    return {
        "index_path": index_path,
        "data_path": data_path,  # where to download/find data
        "transformed_data_path": str(os.path.join(result_folder_path, 'transformed_data.pkl')),  # where to store prepared data
        "predictor_logits_path": str(os.path.join(result_folder_path, "predictor_logits.pkl")),
        "uncertainty_scores_path": str(os.path.join(result_folder_path, "uncertainty_scores.pkl")),
        "experiment_data_path": os.path.join(result_folder_path, "experiment_data.pkl"),
        "html_results_path": os.path.join(result_folder_path, "result.html")
    }


def construct_config_hash(config):
    """
    Generates truncated SHA-256 hashes for each major stage of an experiment, excluding evaluation.

    An experiment consists of four parts:
      1) Data preprocessing
      2) Predictor training
      3) Uncertainty model training
      4) Evaluation

    This function creates three truncated SHA-256 hashes corresponding to:
      - Data preprocessing + predictor training
      - Data preprocessing + predictor training + uncertainty model training

    These hashes allow you to detect changes in each stage’s configuration and decide
    whether a particular stage’s results need to be re-generated. Since the evaluation stage
    always runs, it is omitted from the hashing.

    Hashes are truncated to a precision of hash_length characters for brevity.
    """
    data_management = copy.deepcopy(config['data_management'])
    del data_management['results_path']

    data_config_str = yaml.dump(data_management, sort_keys=True)
    predictor_config_str = yaml.dump(config['train_predictor'], sort_keys=True)
    uncertainty_config_str = yaml.dump(config['train_uncertainty'], sort_keys=True)

    predictor_hash = hashlib.sha256((data_config_str + predictor_config_str).encode("utf-8")).hexdigest()
    uncertainty_hash = hashlib.sha256((data_config_str + predictor_config_str + uncertainty_config_str).encode("utf-8")).hexdigest()

    hash_length = 12
    return predictor_hash[:hash_length], uncertainty_hash[:hash_length]


