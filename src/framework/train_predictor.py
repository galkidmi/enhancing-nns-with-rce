"""
This module defines the experiment pipeline for training and evaluating prediction models.
"""
import torch.nn.functional as F
from copy import deepcopy

import pickle
import torch


def conduct_training(predictor_data, dataset):
    """
    Conducts the training and evaluation workflow for a given model configuration.
    """
    initial_model_state = deepcopy(predictor_data['model'].state_dict())
    initial_optimizer_state = deepcopy(predictor_data['optimizer'].state_dict())

    predictor_logits = {}
    for split_idx, split in dataset.items():
        data_trn = split['trn1']
        data_val = split['val1']

        predictor_data['model'].load_state_dict(initial_model_state)
        predictor_data['optimizer'].load_state_dict(initial_optimizer_state)

        predictor = train_predictor(predictor_data, data_trn, data_val)
        predictor_logits[split_idx] = evaluate_on_all_data(predictor, split)

    with open(predictor_data['predictor_logits_path'], 'wb') as file:
        pickle.dump(predictor_logits, file)


def train_predictor(predictor_data, data_trn, data_val):
    """
    This function trains a predictor (neural network) for a maximum number of epochs,
    evaluating on the validation set after each epoch. If the validation loss does not improve
    for `predictor_config.patience` consecutive epochs, training is stopped early and the model
    reverts to the best known weights.
    """
    model = predictor_data['model']
    optimizer = predictor_data['optimizer']
    true_loss = predictor_data['true_loss']
    loss_function = predictor_data['loss_function']
    max_epochs = predictor_data['max_epochs']
    patience = predictor_data['patience']

    # Store best model state and metrics
    best_weights = deepcopy(model.state_dict())
    best_loss = float('inf')
    patience_counter = 0

    for epoch in range(max_epochs):
        train_one_epoch(model, optimizer, loss_function, data_trn)
        val_loss, val_accuracy = validate(model, true_loss, data_val)

        print(f"Epoch [{epoch+1}/{max_epochs}] "
              f"- Val Loss: {val_loss:.4f} "
              f"- Val Accuracy: {val_accuracy*100:.2f}%")

        # Check if this is the best model so far
        if val_loss < best_loss:
            best_loss = val_loss
            best_weights = deepcopy(model.state_dict())
            patience_counter = 0
        else:
            patience_counter += 1
            if patience_counter >= patience:
                break

    print(f"Validation loss of the best found model: {best_loss:.4f}")
    # Restore best model weights
    model.load_state_dict(best_weights)
    return model


def train_one_epoch(model, optimizer, loss_function, data_trn):
    """
    Performs one epoch of training on the given model.
    """
    model.train()
    for images, labels in data_trn:
        optimizer.zero_grad()
        logits = model(images)
        loss = loss_function(logits, labels)
        loss.backward()
        optimizer.step()


def validate(model, true_loss, data_val):
    """
    Validates the model on a given validation dataset.
    """
    model.eval()
    total_loss = 0.0
    correct = 0
    total_samples = 0

    with torch.no_grad():
        for images, labels in data_val:
            logits = F.softmax(model(images), dim=-1)
            loss = true_loss(logits, labels)
            total_loss += loss.mean()

            _, predicted = torch.max(logits.data, dim=1)
            correct += (predicted == labels).sum().item()
            total_samples += len(images)

    avg_loss = total_loss / total_samples if total_samples else 0.0
    accuracy = correct / total_samples if total_samples else 0.0
    return avg_loss, accuracy


def evaluate_on_all_data(predictor, dataset):
    all_logits = {}
    predictor.eval()
    for loader_name, loader in dataset.items():
        loader_logits = []
        for images, labels in loader:
            logits = F.softmax(predictor(images), dim=-1)
            loader_logits.append((logits, labels))
        all_logits[loader_name] = loader_logits
    return all_logits

