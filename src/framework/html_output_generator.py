from matplotlib import pyplot as plt

import mpld3
import torch
import math

from config_validator import SPLIT_FIELDS


MAX_PAIRS_PER_PLOT = 3
H1_SIZE = 35


def generate_output(html_data, experiment_data):
    accuracies = experiment_data['accuracies']
    deviation = experiment_data['deviation']
    selective_risks = experiment_data['selective_risks']
    expected_risks = experiment_data['expected_risks']

    html_elements = [
        construct_description(html_data),
        construct_accuracy_table(accuracies, deviation),
        plot_risk_coverage_curves(selective_risks, expected_risks)
    ]

    with open(html_data['html_results_path'], "w") as file:
        for html_element in html_elements:
            file.write(html_element)


def construct_description(html_data):
    um_param_row = "" if "um_param_info" not in html_data else \
        f'<p><span class="label">{html_data["um_param_info"]["name"]}:</span> {html_data["um_param_info"]["values"]}</p>'
    return f"""
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            body {{
                font-family: 'Arial', sans-serif;
                margin: 0;
                padding: 0;
                background-color: #fff;
                color: #000;
            }}
            .container {{
                max-width: 1000px;
                margin: 50px auto;
                padding: 30px;
                border: 1px solid #ccc;
                border-radius: 8px;
                text-align: left;
                line-height: 1.8;
            }}
            h1 {{
                text-align: center;
                font-size: {H1_SIZE}px;
                font-weight: bold;
                margin-bottom: 20px;
            }}
            p {{
                margin: 10px 0;
                font-size: 14px;
            }}
            .label {{
                display: inline-block;
                width: 200px;
                font-weight: bold;
            }}
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Experiment Description</h1>
            <p><span class="label">Dataset:</span> {html_data['dataset_name']}</p>
            <p><span class="label">Predictor:</span> {html_data['predictor_name']}</p>
            <p><span class="label">Uncertainty Model:</span> {html_data['uncertainty_name']}</p>
            <p><span class="label">True predictor loss:</span> {html_data['true_loss']}</p>
            {um_param_row}
        </div>
    </body>
    </html>
    """


def construct_accuracy_table(accuracies, deviation):
    html_lines = [
        f'''
            <div style="text-align: center; margin: 10px 0;">
                <h1 style="font-size: 30px; font-weight: bold;">Predictor's Accuracy</h1>
            </div>
            '''
    ]
    # Wrap table in a center-aligned div, or directly set table style
    html_lines.append("<div style='margin: 0 auto; text-align: center;'>")
    html_lines.append("<table border='1' cellspacing='0' cellpadding='5' style='margin: 0 auto;'>")
    html_lines.append("<tr><th></th>" + "".join(f"<th>{field}</th>" for field in SPLIT_FIELDS) + "</tr>")

    # Row: average accuracies
    row_acc = "".join(f"<td>{accuracies[field]:.2f}</td>" for field in SPLIT_FIELDS)
    html_lines.append(f"<tr><td>Avg accuracy</td>{row_acc}</tr>")

    # Row: average deviations
    row_dev = "".join(f"<td>{deviation[field]:.2f}</td>" for field in SPLIT_FIELDS)
    html_lines.append(f"<tr><td>Avg accuracy deviation</td>{row_dev}</tr>")

    # Close table & container
    html_lines.append("</table>")
    html_lines.append("</div>")

    # Join into a single HTML string
    html_table = "\n".join(html_lines)
    return html_table


def plot_risk_coverage_curves(selective_risks, expected_risks):
    risk_coverage_html = ""
    for split_field in SPLIT_FIELDS:
        risk_coverage_html += f'''
        <div style="text-align: center; margin: 10px 0;">
            <h1 style="font-size: 30px; font-weight: bold;">Risk-Coverage Curves for {split_field.upper()} Set</h1>
        </div>
        '''
        risk_coverage_html += plot_field_risk_coverage_curve(selective_risks[split_field], expected_risks[split_field])
        risk_coverage_html += plot_AuRC_table(selective_risks[split_field], expected_risks[split_field])
    return risk_coverage_html


def plot_field_risk_coverage_curve(selective_risks, expected_risks):
    """
    Plots mean and quartile bands for both selective_risks and expected_risks
    as a function of coverage, then returns the plot as HTML via mpld3.

    Args:
        selective_risks (torch.Tensor): shape (runs, points)
        expected_risks  (torch.Tensor): shape (runs, points)

    Returns:
        str: HTML string containing the embedded mpld3 figure.
    """
    # 1) Compute mean & quartiles along dim=0 (the 'runs' dimension)
    mean_sel = selective_risks.mean(dim=0)
    q25_sel  = selective_risks.quantile(0.25, dim=0)
    q75_sel  = selective_risks.quantile(0.75, dim=0)

    mean_exp = expected_risks.mean(dim=0)
    q25_exp  = expected_risks.quantile(0.25, dim=0)
    q75_exp  = expected_risks.quantile(0.75, dim=0)

    # 2) Coverage array (points dimension is size(1))
    n_points = selective_risks.size(1)
    coverage = torch.linspace(0, 1, steps=n_points)

    # 3) Plot
    plt.figure(figsize=(6, 4))

    # Selective (green)
    plt.plot(coverage, mean_sel, label='Mean Selective Risk', color='green', linestyle='-')
    plt.fill_between(coverage, q25_sel, q75_sel, alpha=0.7, color='green', label='25%-75% Range (Selective)')

    # Expected (red)
    plt.plot(coverage, mean_exp, label='Mean Expected Risk', color='red', linestyle='-')
    plt.fill_between(coverage, q25_exp, q75_exp, alpha=0.7, color='red', label='25%-75% Range (Expected)')

    plt.xlabel('Coverage', fontsize=12)
    plt.ylabel('Selective Risk', fontsize=12)
    plt.legend()
    plt.grid(True, linestyle="--", alpha=0.7)
    plt.tight_layout()

    # 4) Convert plot to HTML with mpld3
    fig = plt.gcf()
    html_content = f"""
    <div style="margin: 20px auto; text-align: center; max-width: 800px;">
        {mpld3.fig_to_html(fig)}
    </div>
    """
    plt.close(fig)

    return html_content


def plot_AuRC_table(measured_risks, reference_risks):
    """
    For a given 'field' (e.g. 'trn1'/'val1'), computes the mean & std of measured risks,
    reference risks, and their difference across multiple splits.

    Returns an HTML string with a small heading and a 3-column table:
      Type | Mean | Std
    """
    measured_aurcs = []
    reference_aurcs = []
    for m_arr, r_arr in zip(measured_risks, reference_risks):
        m_tensor = m_arr if isinstance(m_arr, torch.Tensor) else torch.tensor(m_arr)
        r_tensor = r_arr if isinstance(r_arr, torch.Tensor) else torch.tensor(r_arr)

        measured_aurcs.append(m_tensor.mean())
        reference_aurcs.append(r_tensor.mean())

    differences = [m - r for m, r in zip(measured_aurcs, reference_aurcs)]

    measured_t = torch.stack(measured_aurcs)
    reference_t = torch.stack(reference_aurcs)
    difference_t = torch.stack(differences)

    measured_mean = measured_t.mean().item()
    measured_std  = measured_t.std().item()
    reference_mean = reference_t.mean().item()
    reference_std  = reference_t.std().item()
    difference_mean = difference_t.mean().item()
    difference_std  = difference_t.std().item()

    return f"""
    <div style="margin: 10px auto; text-align: center;">
      <table border="1" style="border-collapse: collapse; margin: 0 auto; width: 40%;">
        <thead>
          <tr>
            <th>Type</th>
            <th>Mean AuRC</th>
            <th>Std AuRC</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Measured</td>
            <td>{measured_mean:.4f}</td>
            <td>{measured_std:.4f}</td>
          </tr>
          <tr>
            <td>Reference</td>
            <td>{reference_mean:.4f}</td>
            <td>{reference_std:.4f}</td>
          </tr>
          <tr>
            <td>Difference</td>
            <td>{difference_mean:.4f}</td>
            <td>{difference_std:.4f}</td>
          </tr>
        </tbody>
      </table>
    </div>
    """


### To be changed
def generate_complex_output(complex_html_data, configs_experiment_data):
    accuracies, selective_risks, expected_risks = unpack_exp_data(configs_experiment_data)

    complex_html_elements = []
    complex_html_elements.append(construct_description(complex_html_data))
    complex_html_elements.append(construct_accuracy_table(accuracies[0], complex_html_data))
    complex_html_elements.append(plot_complex_risk_coverage_curves(selective_risks, expected_risks, complex_html_data))

    with open(complex_html_data['html_results_path'], "w") as file:
        for html_element in complex_html_elements:
            file.write(html_element)


def unpack_exp_data(exp_data):
    accuracies, selective_risks, expected_risks = [], [], []
    for data in exp_data:
        accuracies.append(data['accuracies'])
        selective_risks.append(data['selective_risks'])
        expected_risks.append(data['expected_risks'])
    return accuracies, selective_risks, expected_risks


def plot_complex_risk_coverage_curves(selective_risks, expected_risks, complex_html_data):
    splits_num = complex_html_data['splits_num']
    complex_risk_coverage_html = ""

    for split_field in SPLIT_FIELDS:
        complex_risk_coverage_html += f'''
        <div style="text-align: center; margin: 10px 0;">
            <h1 style="font-size: {H1_SIZE}px; font-weight: bold;">Risk-Coverage Curves for {split_field.upper()} Set</h1>
        </div>
        '''

        # plot individual risk coverage curves
        if 'env_param' in complex_html_data:
            for idx, (selective_risk, expected_risk) in enumerate(zip(selective_risks, expected_risks)):
                complex_risk_coverage_html += f'''
                <div style="text-align: center; margin: 10px 0;">
                    <h2 style="font-size: 45; font-weight: bold;">Individual risk-coverage curves for {complex_html_data['order'][idx]}</h2>
                </div>
                '''

                individual_rc = plot_subsplit_risk_coverage_curves(selective_risk, expected_risk, splits_num, split_field)
                complex_risk_coverage_html += individual_rc

                aurcs = compute_AuRCs(selective_risk, expected_risk, split_field, splits_num)
                complex_risk_coverage_html += aurcs

        complex_risk_coverage_html += f'''
        <div style="text-align: center; margin: 10px 0;">
            <h2 style="font-size: 45; font-weight: bold;">Comparison of risk-coverage curves</h2>
        </div>
        '''

        compared_rc = plot_risk_coverage_curves_for_comparison(selective_risks, expected_risks, complex_html_data, split_field)
        complex_risk_coverage_html += compared_rc

        compared_aurcs = compute_AuRCs_for_comparison(selective_risks, complex_html_data, split_field)
        complex_risk_coverage_html += compared_aurcs

        complex_risk_coverage_html += f'''
        <div style="text-align: center; margin: 10px 0;">
            <h2 style="font-size: 45; font-weight: bold;">Deviation of computed AuRC form expected AuRC</h2>
        </div>
        '''

        comparison_table = plot_AuRCs_difference_comparison(selective_risks, expected_risks, complex_html_data, split_field)
        complex_risk_coverage_html += comparison_table

    return complex_risk_coverage_html


def plot_subsplit_risk_coverage_curves(selective_risks, expected_risks, splits_num, split_field):
    is_single_graph = splits_num > 1
    num_figures = math.ceil(splits_num / MAX_PAIRS_PER_PLOT) + is_single_graph  # + 1 for all grpahs at one plot

    # Create a single plot with multiple figures
    fig, axs = plt.subplots(1, num_figures, figsize=(25, 5))

    # Plot individual groups of pairs
    for figure_idx in range(num_figures - is_single_graph):
        start_idx = figure_idx * MAX_PAIRS_PER_PLOT
        end_idx = min(start_idx + MAX_PAIRS_PER_PLOT, splits_num)

        ax = axs[figure_idx] if num_figures > 1 else axs

        add_grouped_graphs(selective_risks, expected_risks, start_idx, end_idx, split_field, ax)

        ax.set_xlabel("Coverage [%]", fontsize=12)
        ax.set_ylabel("Selective Risk", fontsize=12)
        ax.grid(True, linestyle="--")
        ax.legend()

    ax_last = axs[-1] if num_figures > 1 else axs
    add_all_graphs(selective_risks, expected_risks, split_field, ax_last, splits_num)

    ax_last.set_xlabel("Coverage [%]", fontsize=12)
    ax_last.set_ylabel("Selective Risk", fontsize=12)
    ax_last.grid(True, linestyle="--", alpha=0.7)
    ax_last.legend()

    plt.tight_layout()
    # Wrap the graph in a centered div with proper CSS
    subsplit_rc_html = f'''
        <div style="overflow-x: auto; margin: 20px 0;">
            {mpld3.fig_to_html(fig)}
        </div>
        '''
    plt.close(fig)

    return subsplit_rc_html


def add_grouped_graphs(selective_risks_group, expected_risks_group, start_idx, end_idx, subsplit_name, ax):
    for split_idx in range(start_idx, end_idx):
        selective_risk = selective_risks_group[split_idx][subsplit_name]
        expected_risk = expected_risks_group[split_idx][subsplit_name]
        x_coords = torch.arange(len(selective_risk)) / len(selective_risk)

        ax.plot(x_coords, selective_risk, label=f"Measured split {split_idx}", linewidth=2)
        ax.plot(x_coords, expected_risk, label=f"Reference split {split_idx}", linestyle="--", linewidth=2)


def add_all_graphs(selective_risks_group, expected_risks_group, split_field, ax, splits_num):
    all_selective_risks = []
    all_expected_risks = []

    for split_idx in range(splits_num):
        selective_risk = selective_risks_group[split_idx][split_field]
        expected_risk = expected_risks_group[split_idx][split_field]
        x_coords = torch.arange(len(selective_risk)) / len(selective_risk)

        ax.plot(x_coords, selective_risk, label=f"Selective Risk ({len(selective_risks_group.keys())})" if split_idx == 0 else "_", linewidth=1, color="blue")
        ax.plot(x_coords, expected_risk, label=f"Expected Risk ({len(expected_risks_group.keys())})" if split_idx == 0 else "_", linewidth=1, color="orange")

        all_selective_risks.append(selective_risk)
        all_expected_risks.append(expected_risk)

    if len(all_selective_risks) > 1:
        mean_selective_risk = torch.mean(torch.stack(all_selective_risks), dim=0)
        mean_expected_risk = torch.mean(torch.stack(all_expected_risks), dim=0)

        x_coords_selective_risk_mean = torch.arange(len(mean_selective_risk)) / len(mean_selective_risk)
        x_coords_expected_risk_mean = torch.arange(len(mean_expected_risk)) / len(mean_expected_risk)

        plt.plot(x_coords_selective_risk_mean, mean_selective_risk, label="Measured mean", linewidth=4, color="green")
        plt.plot(x_coords_expected_risk_mean, mean_expected_risk, label="Reference mean", linewidth=4, color="red")


def plot_risk_coverage_curves_for_comparison(selective_risks_list, expected_risks_list, complex_html_data, split_field):
    """
    Plots selective risk coverage curves side by side for all models for each split individually.
    """
    splits_num = complex_html_data['splits_num']
    order = complex_html_data['order']

    # Create a single row of subplots for side-by-side visualization
    fig_width_per_split = 6  # Increase per-split width
    fig_height = 5  # Fixed height for readabilit
    fig, axs = plt.subplots(1, splits_num, figsize=(splits_num * fig_width_per_split, fig_height))

    if splits_num == 1:
        axs = [axs]  # Ensure axs is iterable even for a single split

    for split_idx in range(splits_num):
        ax = axs[split_idx]

        # Plot selective risk curves for all models for this split
        for model_idx in range(len(selective_risks_list)):
            selective_risk = selective_risks_list[model_idx][split_idx][split_field]

            x_coords = torch.arange(len(selective_risk)) / len(selective_risk)

            ax.plot(x_coords, selective_risk, label=f"Selective risk for {order[model_idx]}", linewidth=2)

            if 'env_param' in complex_html_data:
                expected_risk = expected_risks_list[model_idx][split_idx][split_field]
                ax.plot(x_coords, expected_risk, label=f"Expected Risk for {order[model_idx]}", linestyle="--", linewidth=2)
            elif model_idx == 0:
                expected_risk = expected_risks_list[0][split_idx][split_field]
                ax.plot(x_coords, expected_risk, label=f"Expected Risk", linestyle="--", linewidth=3)

        ax.set_title(f"Split {split_idx}")
        ax.set_xlabel("Coverage [%]")
        ax.set_ylabel("Selective Risk")
        ax.grid(True, linestyle="--", alpha=0.7)
        ax.legend()

    plt.tight_layout()

    # Wrap the entire row of graphs in a scrollable div
    all_splits_html = f'''
        <div style="overflow-x: auto; margin: 20px 0;">
            {mpld3.fig_to_html(fig)}
        </div>
        '''
    plt.close(fig)

    return all_splits_html


def compute_AuRCs_for_comparison(selective_risks_list, complex_html_data, split_field):
    """
    Generates an HTML table showing AuRC for each model's risk coverage curve without the subsplit column.
    """
    order = complex_html_data['order']
    splits_num = complex_html_data['splits_num']

    columns = [str(i + 1) for i in range(splits_num)] + ["Mean"]

    # HTML table structure
    aurcs_html = """
    <table border="1" style="border-collapse: collapse; text-align: center; width: 100%;">
        <thead>
            <tr>
                <th rowspan="2">Model</th>
                <th colspan="{}">Split</th>
                <th rowspan="2">Mean</th>
            </tr>
            <tr>
    """.format(len(columns) - 1)

    # Add split headers (Split 1, Split 2, ..., Mean)
    for col in columns[:-1]:  # Exclude "Mean" from the subheader row
        aurcs_html += f"<th>{col}</th>"
    aurcs_html += """
            </tr>
        </thead>
        <tbody>
    """

    # Loop through each model and compute AuRC for each split
    for model_idx, selective_risks in enumerate(selective_risks_list):
        model_aurcs = []
        aurcs_html += f"<tr><td>{order[model_idx]}</td>"

        for split_idx in range(splits_num):
            aurc = torch.mean(selective_risks[split_idx][split_field]).item()
            model_aurcs.append(aurc)
            aurcs_html += f"<td>{aurc:.4f}</td>"

        # Calculate and add the mean AuRC for the model
        mean_aurc = sum(model_aurcs) / len(model_aurcs)
        aurcs_html += f"<td>{mean_aurc:.4f}</td></tr>"

    # Closing the table
    aurcs_html += """
        </tbody>
    </table>
    """

    return aurcs_html


def plot_AuRCs_difference_comparison(selective_risks_list, expected_risks_list, complex_html_data, split_field):
    """
    Generates an HTML table showing the difference between selective and expected AuRC
    for each model across splits.
    """
    order = complex_html_data['order']
    splits_num = complex_html_data['splits_num']

    columns = [str(i + 1) for i in range(splits_num)] + ["Mean"]

    # HTML table structure
    aurcs_html = """
    <table border="1" style="border-collapse: collapse; text-align: center; width: 100%;">
        <thead>
            <tr>
                <th rowspan="2">Model</th>
                <th colspan="{}">Split</th>
                <th rowspan="2">Mean</th>
            </tr>
            <tr>
    """.format(len(columns) - 1)

    # Add split headers (Split 1, Split 2, ..., Mean)
    for col in columns[:-1]:  # Exclude "Mean" from the subheader row
        aurcs_html += f"<th>{col}</th>"
    aurcs_html += """
            </tr>
        </thead>
        <tbody>
    """

    # Loop through each model and compute AuRC differences
    for model_idx, (selective_risks, expected_risks) in enumerate(zip(selective_risks_list, expected_risks_list)):
        model_differences = []
        aurcs_html += f"<tr><td>{order[model_idx]}</td>"

        for split_idx in range(splits_num):
            selective_aurc = torch.mean(selective_risks[split_idx][split_field]).item()
            expected_aurc = torch.mean(expected_risks[split_idx][split_field]).item()
            difference = selective_aurc - expected_aurc
            model_differences.append(difference)
            aurcs_html += f"<td>{difference:.4f}</td>"

        # Calculate and add the mean difference for the model
        mean_difference = sum(model_differences) / len(model_differences)
        aurcs_html += f"<td>{mean_difference:.4f}</td></tr>"

    # Closing the table
    aurcs_html += """
        </tbody>
    </table>
    """

    return aurcs_html


def plot_dependency_of_um_on_env_parameter(selective_risks_list, expected_risks_lists, complex_html_data):
    splits_num = complex_html_data['splits_num']

    dependencies_html_graph = f'''
    <div style="text-align: center; margin: 10px 0;">
        <h1 style="font-size: {H1_SIZE}px; font-weight: bold;">
            Dependency of uncertainty model performance on {complex_html_data['env_param']['name']}
        </h1>
    </div>
    '''

    env_values, _ = torch.sort(torch.tensor(complex_html_data['actual_env_param']['values']))

    for split_field in SPLIT_FIELDS:
        dependencies_html_graph += f'''
        <div style="text-align: center; margin: 10px 0;">
            <h2 style="font-size: 30px; font-weight: bold;">{split_field} Set</h2>
        </div>
        '''

        fig_width_per_split = 6  # Increase per-split width
        fig_height = 5  # Fixed height for readabilit
        fig, axs = plt.subplots(1, splits_num, figsize=(splits_num * fig_width_per_split, fig_height))

        if splits_num == 1:
            axs = [axs]  # Ensure axs is iterable

        for split_idx in range(splits_num):
            aurcs = []  # Reset for each split

            for model_idx in range(len(selective_risks_list)):
                actual_aurc = torch.mean(selective_risks_list[model_idx][split_idx][split_field]).item()
                expected_aurc = torch.mean(expected_risks_lists[model_idx][split_idx][split_field]).item()
                deviation = actual_aurc - expected_aurc
                aurcs.append(deviation)

            ax = axs[split_idx]

            ax.plot(env_values, aurcs, linewidth=2, marker='s')

            ax.set_title(f"Split {split_idx}")
            ax.set_xlabel(f"{complex_html_data['env_param']['name']}", fontsize=12)
            ax.set_ylabel("Deviation of AuRC from expected AuRC", fontsize=12)
            ax.grid(True, linestyle="--")

        plt.tight_layout()
        dependencies_html_graph += f'''
            <div style="overflow-x: auto; margin: 20px 0;">
                {mpld3.fig_to_html(fig)}
            </div>
            '''
        plt.close(fig)

    return dependencies_html_graph

