from copy import deepcopy
import torch


def get_uncertainty_scores(uncertainty_data, dataset):
    if 'optimizer' not in set(uncertainty_data.keys()):
        return conduct_intristic_model_evaluation(uncertainty_data)
    else:
        return conduct_learning_model_training(uncertainty_data, dataset)


def conduct_intristic_model_evaluation(uncertainty_data):
    uncertainty_scores = {}
    for split_idx, split in uncertainty_data['predictor_logits'].items():
        uncertainty_scores[split_idx] = evaluate_intristic_model(uncertainty_data['model'], split)
    return uncertainty_scores


def evaluate_intristic_model(uncertainty_model, logits):
    all_logits = {}
    for loader_name, loader in logits.items():
        loader_scores = []
        for batch, labels in loader:
            print(batch.shape)
            scores = uncertainty_model(batch)
            loader_scores.append((scores, labels))
        all_logits[loader_name] = loader_scores
    return all_logits


def conduct_learning_model_training(uncertainty_data, dataset):
    """
    Conducts the training and evaluation workflow for a given uncertainty model configuration.
    """
    uncertainty_scores = {}
    initial_model_state = deepcopy(uncertainty_data['model'].state_dict())
    initial_optimizer_state = deepcopy(uncertainty_data['optimizer'].state_dict())

    for split_idx, split in dataset.items():
        split_predictor_logits = uncertainty_data['predictor_logits'][split_idx]

        uncertainty_data['model'].load_state_dict(initial_model_state)
        uncertainty_data['optimizer'].load_state_dict(initial_optimizer_state)

        uncertainty_model = train_uncertainty(uncertainty_data, split_predictor_logits, split)

        uncertainty_model.eval()
        uncertainty_scores[split_idx] = evaluate_learning_model(uncertainty_data['loss_function_name'], uncertainty_model, split)

    return uncertainty_scores


def train_uncertainty(uncertainty_data, predictors_logits, dataset):
    """
    This function trains an uncertainty model (neural network) for a maximum number of epochs,
    evaluating on the validation set after each epoch. If the validation loss does not improve
    for `predictor_config.patience` consecutive epochs, training is stopped early and the model
    reverts to the best known weights.
    """
    model = uncertainty_data['model']
    max_epochs = uncertainty_data['max_epochs']
    patience = uncertainty_data['patience']

    data_trn = dataset['trn2']
    data_val = dataset['val2']
    logits_trn = predictors_logits['trn2']
    logits_val = predictors_logits['val2']

    # Store best model state and metrics
    best_weights = deepcopy(model.state_dict())
    best_loss = float('inf')
    patience_counter = 0

    for epoch in range(max_epochs):
        train_loss = train_one_epoch(uncertainty_data, model, data_trn, logits_trn)
        val_loss = validate(uncertainty_data, model, data_val, logits_val)

        print(f"Epoch [{epoch + 1}/{max_epochs}] "
              f"- Train Loss: {train_loss:.10f} "
              f"- Val Loss: {val_loss:.10f}")

        # Check if this is the best model so far
        if val_loss < best_loss:
            best_loss = val_loss
            best_weights = deepcopy(model.state_dict())
            patience_counter = 0
        else:
            patience_counter += 1
            if patience_counter >= patience:
                break

    print(f"Validation loss of the best found model: {best_loss:.8f}")
    # Restore best model weights
    model.load_state_dict(best_weights)
    return model


def train_one_epoch(uncertainty_data, model, data, logits):
    """
    Performs one epoch of training on the given model.
    """
    optimizer = uncertainty_data['optimizer']
    predictor_true_loss = uncertainty_data['true_loss']
    loss_function = uncertainty_data['loss_function']

    running_loss = 0.0
    total_samples = 0

    model.train()
    for (images, labels), (predictor_logits, predictor_labels) in zip(data, logits):
        if torch.any(labels != predictor_labels):
            message = "Predictor's logits were stored in different order than the computation of uncertainty scores requires!"
            raise ValueError(message)

        optimizer.zero_grad()
        uncertainty_scores = model(images)
        if uncertainty_data['loss_function_name'] == 'TCP_score':
            loss = loss_function(uncertainty_scores, predictor_logits, labels)
        else:
            approximated_risk = predictor_true_loss(predictor_logits, labels)
            loss = loss_function(approximated_risk, uncertainty_scores)
        loss.backward()

        for name, param in model.named_parameters():
            if param.grad is None:
                print(f"{name}: No gradient computed")
            elif param.grad.norm().item() == 0:
                print("Zero gradient!")
            # elif running_loss == 0.0:
            #     print(f"loss: {loss.item()}")
            #     print(f"grad: {param.grad}")

        optimizer.step()

        running_loss += loss.item() * len(labels)
        total_samples += len(labels)

    return running_loss / total_samples


def validate(uncertainty_data, model, data, logits):
    """
    Validates the model on a given validation dataset.
    """
    predictor_true_loss = uncertainty_data['true_loss']
    loss_function = uncertainty_data['loss_function']

    total_loss = 0
    total_samples = 0

    model.eval()
    with torch.no_grad():
        for (images, labels), (predictor_logits, predictor_labels) in zip(data, logits):
            if torch.any(labels != predictor_labels):
                message = "Predictor's logits were stored in different order than the computation of uncertainty scores requires!"
                raise ValueError(message)

            uncertainty_scores = model(images)
            approximated_risk = predictor_true_loss(predictor_logits, labels)
            if uncertainty_data['loss_function_name'] == 'TCP_score':
                loss = loss_function(uncertainty_scores, predictor_logits, labels)
            else:
                loss = loss_function(approximated_risk, uncertainty_scores)
            total_loss += len(images) * loss
            total_samples += len(images)

    avg_loss = total_loss / total_samples if total_samples else 0.0
    return avg_loss


def evaluate_learning_model(loss_function_name, uncertainty_model, dataset):
    # uncertainty model is already in evaluation mode from validation phase
    all_logits = {}
    for loader_name, loader in dataset.items():
        loader_scores = []
        for images, labels in loader:
            scores = uncertainty_model(images)
            if loss_function_name == 'TCP_score':
                scores = 1 - scores
            loader_scores.append((scores, labels))
        all_logits[loader_name] = loader_scores
    return all_logits
