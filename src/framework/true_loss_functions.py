import torch.nn.functional as F
import torch


class MatrixLoss:
    def __init__(self, matrix):
        self.matrix = matrix

    def __call__(self, probs, labels):
        batch_size = probs.shape[0]

        # Gather the loss values from the matrix based on true labels
        loss_per_sample = torch.zeros(batch_size, device=probs.device)

        for i in range(batch_size):
            true_label = labels[i].item()  # Extract the true label for this sample
            loss_per_sample[i] = torch.sum(self.matrix[:, true_label] * probs[i])  # Compute expected errorfor the sample

        return loss_per_sample


def zero_one_loss(logits, labels):
    predictions = torch.argmax(logits, dim=-1)
    return (predictions != labels).int()


def mean_absolute_error(logits, labels):
    one_hot = F.one_hot(labels, num_classes=logits.shape[-1]).float()  # One-hot encode ground truth
    loss_per_class = torch.abs(logits - one_hot)  # Loss for each class
    return torch.sum(logits * loss_per_class, dim=-1)  # Sum over all classes


def mean_squared_error(logits, labels):
    one_hot = F.one_hot(labels, num_classes=logits.shape[-1]).float()  # One-hot encode ground truth
    loss_per_class = (logits - one_hot) ** 2  # Loss for each class
    return torch.sum(logits * loss_per_class, dim=-1)  # Sum over all classes

