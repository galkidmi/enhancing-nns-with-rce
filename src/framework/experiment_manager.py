import config_dispatcher as cd
import html_output_generator as html
import file_system_manager as fsm
import config_validator as cval
import dataset_manager as dm
import train_predictor as pred
import train_uncertainty as unc
import evaluator as ev

import pickle
import copy
import yaml


def run_experiment(config_path):
    config = load_config(config_path)
    flat_config = flatten_config(config)

    cval.validate_config_structure(flat_config)
    cval.validate_config_content(flat_config)
    comp_params = cval.get_comparison_parameters(flat_config)

    index = fsm.fetch_prepared_index(flat_config["data_management.results_path"])

    simple_flat_configs = generate_simplified_configs(flat_config, comp_params)

    if len(simple_flat_configs) > 1:
        print(f'Entering complex experiment section with {len(simple_flat_configs)} simple configs')
        disable_html_output(simple_flat_configs)
        html_results_path = fsm.prepare_result_paths(config_path, config, simple_flat_configs)
        order = cval.get_configs_execution_order(flat_config)  # TODO: rewrite and validate this part
        complex_html_data = cd.fetch_complex_html_part(config, comp_params, html_results_path, order)  # TODO: rewrite and validate this part
        conduct_complex_experiment(index, complex_html_data, simple_flat_configs)
    else:
        conduct_simple_experiment(index, config_path, config)


def load_config(config_path):
    """Loads the YAML configuration file."""
    try:
        with open(config_path, 'r') as file:
            return yaml.safe_load(file)
    except Exception as e:
        raise RuntimeError(f"Failed to load configuration from {config_path}: {e}")


def flatten_config(config, parent_key='', sep='.'):
    flat_dict = {parent_key: set(config.keys())} if parent_key else {'root': set(config.keys())}

    for key, value in config.items():
        new_key = f"{parent_key}{sep}{key}" if parent_key else key
        if isinstance(value, dict) and new_key != "data_management.splits":
            flat_dict.update(flatten_config(value, new_key, sep))
        else:
            flat_dict[new_key] = value
    return flat_dict


def unflatten_config(flat_dict, sep='.'):
    """
    Reconstructs a nested dictionary from a flattened dictionary.
    """
    unflattened = {}
    for dict_path, children in flat_dict.items():
        if dict_path == 'root':
            continue

        keys = dict_path.split(sep)
        iter_dict = unflattened
        for key in keys:  # traverse dict path
            if key == keys[-1]:  # last key in dict path
                if isinstance(children, set):  # this is not the leaf -> expand dictionary
                    iter_dict[key] = {child: {} for child in children}
                else:  # this is the leaf -> add the value to the key
                    iter_dict[key] = children
                break
            elif key not in iter_dict:  # this key does not exist yet and middle of the path ->  add the key
                iter_dict[key] = {}
            iter_dict = iter_dict[key]  # step one level lower
    return unflattened


def generate_simplified_configs(config, comp_params):
    """
    Generates simplified configs based on comp_params rules.
    """
    # Simplify config based on comp_params
    if len(comp_params) == 2:  # uncertainty model name and uncertainty loss function are arrays
        return simplify_um_models_part(config, comp_params)
    elif len(comp_params) == 1:
        return simplify_config(config, comp_params)
    else:
        return []


def simplify_config(config, comp_params):
    """
    Simplifies the config by expanding any parameter in params_group that is defined as an array.
    Only one parameter in params_group can be an array (except for specific exceptions), which is already guaranteed by
    config_validator at this time.
    """
    if not comp_params:  # there is no parameters that are defined as an array
        return [config]

    new_configs = []
    param_name = comp_params[0]['name']
    for value in comp_params[0]['values']:
        new_config = copy.deepcopy(config)
        new_config[param_name] = value
        new_configs.append(new_config)

    return new_configs


def simplify_um_models_part(config, comp_params):
    """
    Expands the uncertainty model parameters (comp_params) into multiple configs
    if `train_uncertainty.name` and `train_uncertainty.loss_function` are both arrays, generating all combinations.
    """
    # Handle the special case for `name` and `loss_function`
    if comp_params[0]['name'] == "train_uncertainty.name":
        name_values = comp_params[0]["values"]
        loss_function_values = comp_params[1]["values"]
    else:
        name_values = comp_params[1]["values"]
        loss_function_values = comp_params[0]["values"]

    # Generate all combinations of `name` and `loss_function`
    new_configs = []
    for name in name_values:
        if name != 'MCP_score':
            for loss_function in loss_function_values:
                new_config = copy.deepcopy(config)
                new_config["train_uncertainty.name"] = name
                new_config["train_uncertainty.loss_function"] = loss_function
                new_configs.append(new_config)
        else:
            new_config = copy.deepcopy(config)
            new_config["train_uncertainty.name"] = name
            del new_config["train_uncertainty.loss_function"]
            del new_config["train_uncertainty.optimizer"]
            del new_config["train_uncertainty.learning_rate"]
            del new_config["train_uncertainty.max_epochs"]
            del new_config["train_uncertainty.patience"]

    return new_configs


def conduct_complex_experiment(index, complex_html_data, simple_flat_configs):
    configs_experiment_data = []
    for idx, simple_flat_config in enumerate(simple_flat_configs):
        print(f"Get accuracies and risks from simple config number {idx}")
        simple_config = unflatten_config(simple_flat_config)
        data_for_result_hash = conduct_simple_experiment(index, f"exp{idx}", simple_config)

        with open(index['evaluation'][data_for_result_hash], 'rb') as f:
            experiment_data = pickle.load(f)
        configs_experiment_data.append(experiment_data)

    html.generate_complex_output(complex_html_data, configs_experiment_data)


def disable_html_output(simple_flat_configs):
    """Modifies configs to disable HTML output by setting `evaluation.html_output` to False."""
    for simple_config in simple_flat_configs:
        simple_config["evaluation.html_output"] = False


def conduct_simple_experiment(index, config_path, config):
    predictor_hash, uncertainty_hash = fsm.construct_config_hash(config)
    paths = fsm.construct_paths(config_path, config)

    dataset_data = cd.fetch_raw_dataset_data(config, paths)
    wrapped_raw_dataset = dm.fetch_raw_dataset(dataset_data)

    conduct_predictor_training(index, config, paths, wrapped_raw_dataset, predictor_hash)
    conduct_uncertainty_model_training(index, config, paths, wrapped_raw_dataset, predictor_hash, uncertainty_hash)
    conduct_evaluation(index, config, paths, predictor_hash, uncertainty_hash)
    conduct_output_generation(index, config, paths, uncertainty_hash)
    return uncertainty_hash


def conduct_predictor_training(index, config, paths, wrapped_raw_dataset, predictor_hash):
    if predictor_hash in index['predictor']:
        print(f"Using predictor logits from {index['predictor'][predictor_hash]}")
        return

    wrapped_dataset = conduct_dataset_construction(config, wrapped_raw_dataset, 'train_predictor')
    predictor_data = cd.fetch_predictor_data(config, paths, wrapped_dataset.dataset, wrapped_dataset.num_classes)
    pred.conduct_training(predictor_data, wrapped_dataset.dataset)
    index["predictor"][predictor_hash] = predictor_data["predictor_logits_path"]
    with open(paths['index_path'], 'w') as f:
        yaml.dump(index, f, default_flow_style=False)


def conduct_uncertainty_model_training(index, config, paths, wrapped_raw_dataset, predictor_hash, uncertainty_hash):
    if uncertainty_hash in index['uncertainty']:
        print(f"Using uncertainty scores from {index['uncertainty'][uncertainty_hash]}")
        return

    wrapped_dataset = conduct_dataset_construction(config, wrapped_raw_dataset, 'train_uncertainty')
    uncertainty_data = cd.fetch_uncertainty_data(config, paths, wrapped_dataset.dataset, wrapped_dataset.num_classes)
    with open(index["predictor"][predictor_hash], 'rb') as f:
        uncertainty_data['predictor_logits'] = pickle.load(f)

    uncertainty_scores = unc.get_uncertainty_scores(uncertainty_data, wrapped_dataset.dataset)
    data_to_store = {'scores': uncertainty_scores, 'probabilities': wrapped_dataset.probabilities}
    with open(uncertainty_data['uncertainty_scores_path'], 'wb') as file:
        pickle.dump(data_to_store, file)

    index["uncertainty"][uncertainty_hash] = uncertainty_data["uncertainty_scores_path"]
    with open(paths['index_path'], 'w') as f:
        yaml.dump(index, f, default_flow_style=False)


def conduct_dataset_construction(config, wrapped_raw_dataset, config_part_name):
    predictor_dataset_data = cd.fetch_dataset_data(config, config_part_name)
    wrapped_dataset = dm.prepare_dataset(predictor_dataset_data, wrapped_raw_dataset)
    return wrapped_dataset


def conduct_evaluation(index, config, paths, predictor_hash, uncertainty_hash):
    if uncertainty_hash in index["evaluation"]:
        print(f"Using experiment data from {index['evaluation'][uncertainty_hash]}")
        return

    with open(index["predictor"][predictor_hash], 'rb') as f:
        predictor_logits = pickle.load(f)
    with open(index["uncertainty"][uncertainty_hash], 'rb') as f:
        uncertainty_data = pickle.load(f)

    evaluation_data = cd.fetch_evaluation_data(config, paths)
    ev.compute_experiment_data(evaluation_data, predictor_logits, uncertainty_data)

    index["evaluation"][uncertainty_hash] = evaluation_data["experiment_data_path"]
    with open(paths['index_path'], 'w') as f:
        yaml.dump(index, f, default_flow_style=False)


def conduct_output_generation(index, config, paths, uncertainty_hash):
    if "evaluation" in config and "html_output" in config["evaluation"] and not config["evaluation"]["html_output"]:
        return

    with open(index["evaluation"][uncertainty_hash], 'rb') as f:
        experiment_data = pickle.load(f)
    html_data = cd.fetch_html_part(config, paths)
    html.generate_output(html_data, experiment_data)
