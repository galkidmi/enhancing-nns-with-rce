from scipy.stats import multivariate_normal
from torch.utils.data import Dataset
import numpy as np
import re


class GenDataset(Dataset):
    """
    Extracts dimension (X), number of classes (Y), samples per class (Z), linear separability (L - yes, N - no)
    The dataset name should be in the format 'GenXdYcZsW', where X is dimension, Y is number of classes, and Z is number
    of samples per class and W represents the linear separability of the points
    (L - linearly separable, N - linearly non-separable)
    """
    def __init__(self, dataset_name, seed=42):
        """
        Generate n-dimensional linearly separable data points from Gaussian distributions.
        """
        data, labels, probabilities = generate_dataset(dataset_name, seed)
        self.data = data
        self.labels = labels
        self.probabilities = Probabilities(probabilities)

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        """
        Retrieve a sample and its label by index.

        :param idx: Index of the sample.
        :return: Tuple (point, label).
        """
        return self.data[idx], self.labels[idx]


class Probabilities(Dataset):
    def __init__(self, probabilities):
        self.probabilities = probabilities

    def __len__(self):
        return len(self.probabilities)

    def __getitem__(self, idx):
        return self.probabilities[idx]


def generate_dataset(dataset_name, seed):
    np.random.seed(seed)

    data = []
    labels = []
    probabilities = []

    pdfs = []
    dimensions, num_classes, samples_per_class, linearly_separable = extract_dataset_info(dataset_name)
    for label in range(num_classes):
        var = np.array([0.5 for _ in range(dimensions)]) if linearly_separable else (
                                            np.array([np.random.uniform(0.5, 1) for _ in range(dimensions)]))

        step = 4 if linearly_separable else 1
        mean = np.random.uniform(label * step, (label + 1) * step, size=dimensions)

        # Generate points from the Gaussian distribution
        points = np.random.multivariate_normal(mean, np.diag(var), size=samples_per_class)

        pdfs.append(multivariate_normal(mean=mean, cov=np.diag(var)))

        data.append(points)
        labels.append(np.full(samples_per_class, label))

    for labeled_subdataset in data:
        subdataset_probs = []
        for label in range(num_classes):
            subdataset_probs.append(pdfs[label].logpdf(labeled_subdataset))
        probabilities.append(np.vstack(subdataset_probs).astype(np.float32).T)  # shape number of samples, number of labels

    data = np.vstack(data).astype(np.float32)
    labels = np.hstack(labels)
    probabilities = np.vstack(probabilities).astype(np.float32)

    shuffled_indices = np.random.permutation(len(data))
    return data[shuffled_indices], labels[shuffled_indices], probabilities[shuffled_indices]


def extract_dataset_info(dataset_name):
    """
    Extracts dimension (X), number of classes (Y), samples per class (Z), linear separability (L - yes, N - no)
    from the dataset name in the format 'GenXdYcZsW'

    :param dataset_name: String, dataset name (e.g., 'Gen2d4c500L').
    :return: Tuple (X, Y, Z, W) as integers.
    """
    pattern = r"Gen(\d+)d(\d+)c(\d+)([LN])"
    match = re.match(pattern, dataset_name)
    if match:
        dimensions = int(match.group(1))
        num_classes = int(match.group(2))
        samples_per_class = int(match.group(3))
        linearly_separable = dataset_name[-1] == 'L'
        return dimensions, num_classes, samples_per_class, linearly_separable
    else:
        raise ValueError(f"Dataset name '{dataset_name}' does not match the expected format 'GenXdYcZW'.")


