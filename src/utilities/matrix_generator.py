import numpy as np
import yaml

"""
Generates true loss matrices 
"""


size = 4

M = np.ones((size, size), dtype=int).tolist()

for i in range(size):
    M[i][i] = 0

yaml_path = "/configuration_files/loss_matrices/zero_loss_4.yaml"
with open(yaml_path, "w") as f:
    yaml.dump(M, f, default_flow_style=True, width=size*3, indent=1)

